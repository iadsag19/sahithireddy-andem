import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class BinaryHeap<T> implements PQ<T> {
    T[] pq;
    Comparator<T> c;
    int size;
    /** Build a priority queue with a given array q */
    BinaryHeap(T[] q, Comparator<T> comp) {
	pq = q;
	c = comp;
	size = pq.length-1;
	buildHeap();
    }

    /** Create an empty priority queue of given maximum size */
    BinaryHeap(int n, Comparator<T> comp) { /* to be implemented */
    	c = comp;
    	pq=(T[])Array.newInstance(getClass(), n);
    	Scanner s = new Scanner(System.in);
    
    	size = n - 1;
    	buildHeap();
    }
    
    public void displayArray(T[] a){
    	for(int j=1;j<=size;j++)
			System.out.println(a[j]);			    	
    }

    public void insert(T x) {
	add(x);
    }

    public T deleteMin() {
	return remove();
    }

    public T min() { 
	return peek();
    }
    public void resize(){
    	T[] tmp=pq;
    	pq=(T[])Array.newInstance(getClass(),(size+2));
    	pq=Arrays.copyOf(tmp, pq.length);
    }
    public void add(T x) { /* to be implemented */
      if(size>=pq.length-1)
    	  resize();
       size++;
       pq[size] = x;
       percolateUp(size);
       displayArray(pq);
    }

    public T remove() { /* to be implemented */
    	T min = pq[1];
    	pq[1] = pq[size];
    	percolateDown(1);
    	size--;
    	
	return min;
    }

    public T peek() { /* to be implemented */
	return pq[1];
    }

    /** pq[i] may violate heap order with parent */
    void percolateUp(int i) { /* to be implemented */
    	
    	pq[0] = pq[i];
		
		while(c.compare(pq[i/2],pq[0])>0){
			pq[i] = pq[i/2];
			pq[i/2] = pq[0];
			i = i/2;			
		}
    	pq[i] = pq[0];
		
    }

    /** pq[i] may violate heap order with children */
    void percolateDown(int i) { /* to be implemented */
    	    	
		while(2*i<=size && 2*i+1<=size){
			T x = pq[i];
			if(c.compare(x,pq[2*i])>0|| c.compare(x,pq[2*i+1])>0){
				if(c.compare(pq[2*i],pq[2*i+1])<0)
				{
					pq[i] = pq[2*i];
					pq[2*i] = x;
					
				}
				else{
					pq[i] = pq[2*i+1];
					pq[2*i+1] = x;					
				}				
			}
			i=i*2;
		}
		
    }

    /** Create a heap.  Precondition: none. */
    void buildHeap() {
    	int n = size/2;
    	while(n>0){
    		percolateDown(n);
    		n--;
    	}
    	
    }

    /* sort array A[1..n].  A[0] is not used. 
       Sorted order depends on comparator used to build heap.
       min heap ==> descending order
       max heap ==> ascending order
     */
    public static<T> void heapSort(T[] A, Comparator<T> comp) { /* to be implemented */
    	
    	BinaryHeap b = new BinaryHeap(A,comp);
    	
    	for(int i=1;i<A.length;i++){
    	   T temp = (T) b.remove();
    		b.buildHeap();
    		System.out.println("size:"+b.size);
    		A[b.size+1]=temp;
    	}
    	System.out.println("Sorted Array:");
    	b.displayArray(A);
    	
    }
    public static void main(String[] args) {
    	Comparator c = new MyComparator<Integer>();
    	Integer[] pq = {0,40,60,10,20,50,30,5};
		BinaryHeap<Integer> bh = new BinaryHeap<Integer>(pq,c);
		bh.remove();
		bh.add(21);		
		BinaryHeap.heapSort(bh.pq, c);
	}
}
