import java.util.Comparator;

public class MyComparator<T extends Comparable<T>> implements Comparator<T>{

	@Override
	public int compare(T o1, T o2) {
		// TODO Auto-generated method stub
		int n = 0;
		if(o1!=null&&o2!=null){
		 n = o1.compareTo(o2);
		}
		return n;
	}

}
