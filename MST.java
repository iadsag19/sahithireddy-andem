import java.util.PriorityQueue;
import java.util.Scanner;
import java.lang.Comparable;

public class MST {
    static final int Infinity = Integer.MAX_VALUE;

    static int PrimMST(Graph g)
    {
        int wmst = 0;
        Vertex src = g.verts.get(1);
        Vertex[] visited_vertices = new Vertex[g.verts.size()];
        // Code for Prim's algorithm needs to be written
        System.out.println("name:"+src.name);
        	  
        src.seen = true;
		
        PriorityQueue<Edge> pq = new PriorityQueue<Edge>(src.Adj.size());
        for(Edge e:src.Adj){
        	pq.add(e);
        }
        while(!pq.isEmpty()){
        	
        	Edge e = (Edge) pq.remove();
        	System.out.println("in while: removing"+e.From.seen+"--"+e.To.seen);
        	if(e.From.seen&&e.To.seen){
        		System.out.println("extra! should run end lo");
        		continue;
        	}
        	
        	Vertex u = e.From;
        	u.seen = true;
        	System.out.println("should run BEGINNING lo");
        	Vertex v = e.otherEnd(u);
        	
        	v.seen = true;
        	v.parent = src;
        	System.out.println("v.otherEnd"+v.name+"--"+v.seen);
        	wmst = wmst + e.Weight;
        	for(Edge edge:v.Adj){
        		if(!(edge.From.seen&&edge.To.seen)){
        			
        		Vertex w  = edge.otherEnd(v);
        		System.out.println("V.Adj.otherEnd = "+w.name);
        		if(!w.seen){
        			pq.add(edge);
        		}
        		}
        	}
        }
    
        return wmst;
    }

    public static void main(String[] args) 
    {
        Scanner in = new Scanner(System.in);
        Graph g = Graph.readGraph(in, false);
        System.out.println(PrimMST(g));
    }
}
